package com.example.EmployeeLeaveNaufal.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeLeaveNaufal.exceptions.ResourceNotFoundException;
import com.example.EmployeeLeaveNaufal.interfaces.Auditor;
import com.example.EmployeeLeaveNaufal.dtos.PositionLeaveDto;
import com.example.EmployeeLeaveNaufal.models.PositionLeave;
import com.example.EmployeeLeaveNaufal.repositories.PositionLeaveRepository;

@RestController
@RequestMapping("/api/positionLeave")
public class PositionLeaveController implements Auditor {

	ModelMapper modelMapper = new ModelMapper();
	@Autowired
	PositionLeaveRepository positionLeaveRepo;
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createPositionLeave(@Valid @RequestBody PositionLeaveDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		body.setCreatedBy(auditorName);
		body.setUpdatedBy(auditorName);
		PositionLeave posLeaveEntity = modelMapper.map(body, PositionLeave.class);
		positionLeaveRepo.save(posLeaveEntity);
		
		body.setPositionLeaveId(posLeaveEntity.getPositionLeaveId());
		result.put("Message", "Position Leave Created Successfully");
		result.put("Data", body);
		return result;
	}
	
	// READ ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllPositionLeave() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<PositionLeave> listPosLeaveEntity = positionLeaveRepo.findAll();
		List<PositionLeaveDto> listPosLeaveDto = new ArrayList<PositionLeaveDto>();
		
		for (PositionLeave posLeaveEntity : listPosLeaveEntity) {
			PositionLeaveDto posLeaveDto = modelMapper.map(posLeaveEntity, PositionLeaveDto.class);
			listPosLeaveDto.add(posLeaveDto);
		}
		
		result.put("Message", "All Position Leave Read Successfully");
		result.put("Data", listPosLeaveDto);
		result.put("Total", listPosLeaveDto.size());
		return result;
	}
	
	// READ BY ID
	@GetMapping("/read")
	public Map<String, Object> getUserById(@RequestParam(name = "positionLeaveId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		PositionLeave posLeaveEntity = positionLeaveRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("PositionLeave", "positionLeaveId", id));
		PositionLeaveDto posLeaveDto = modelMapper.map(posLeaveEntity, PositionLeaveDto.class);
		
		result.put("Message", "Position Leave Read Successfully");
		result.put("Data", posLeaveDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updatePositionLeave(@Valid @RequestBody PositionLeaveDto body, @RequestParam(name = "positionLeaveId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		body.setCreatedBy(auditorName);
		body.setUpdatedBy(auditorName);
		PositionLeave posLeaveEntity = positionLeaveRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("PositionLeave", "positionLeaveId", id));
		Date createdAt = posLeaveEntity.getCreatedAt();
		posLeaveEntity = modelMapper.map(body, PositionLeave.class);
		posLeaveEntity.setPositionLeaveId(id);
		posLeaveEntity.setCreatedAt(createdAt);
		
		positionLeaveRepo.save(posLeaveEntity);
		
		body.setPositionLeaveId(posLeaveEntity.getPositionLeaveId());
		result.put("Message", "Position Leave Updated Successfully");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deletePositionLeave(@RequestParam(name = "positionLeaveId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		PositionLeave posLeaveEntity = positionLeaveRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("PositionLeave", "positionLeaveId", id));
		
		PositionLeaveDto posLeaveDto = modelMapper.map(posLeaveEntity, PositionLeaveDto.class);
		
		positionLeaveRepo.deleteById(id);
		
		result.put("Message", "Position Leave Deleted Successfully");
		result.put("Data Deleted", posLeaveDto);
		return result;
	}
}
