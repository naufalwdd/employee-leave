package com.example.EmployeeLeaveNaufal.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeLeaveNaufal.dtos.UserLeaveRequestDto;
import com.example.EmployeeLeaveNaufal.exceptions.LacksOfLeaveAllowanceException;
import com.example.EmployeeLeaveNaufal.exceptions.RemainingLeaveException;
import com.example.EmployeeLeaveNaufal.exceptions.RequestDateInputException;
import com.example.EmployeeLeaveNaufal.exceptions.ResourceNotFoundException;
import com.example.EmployeeLeaveNaufal.interfaces.Auditor;
import com.example.EmployeeLeaveNaufal.models.PositionLeave;
import com.example.EmployeeLeaveNaufal.models.UserLeaveRequest;
import com.example.EmployeeLeaveNaufal.models.Users;
import com.example.EmployeeLeaveNaufal.repositories.PositionLeaveRepository;
import com.example.EmployeeLeaveNaufal.repositories.UserLeaveRequestRepository;
import com.example.EmployeeLeaveNaufal.repositories.UsersRepository;

@RestController
@RequestMapping("/api/leave")
public class UserLeaveRequestController implements Auditor {
	ModelMapper modelMapper = new ModelMapper();
	@Autowired
	UsersRepository userRepo;
	@Autowired
	PositionLeaveRepository positionLeaveRepo;
	@Autowired
	UserLeaveRequestRepository userRequestRepo;
	
	// LEAVE REQUEST
	@PostMapping("/request")
	public Map<String, Object> request(@Valid @RequestBody UserLeaveRequestDto body, @RequestParam(name = "userId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> message = new HashMap<String, Object>();
		Users userEntity = userRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Users", "userId", id));
		Date todaysDate = new Date();
		body.setRequestDate(todaysDate);
		body.setCreatedBy(auditorName);
		body.setUpdatedBy(auditorName);
		UserLeaveRequest userLeaveRequest = modelMapper.map(body, UserLeaveRequest.class);
		createRequest(body, userEntity, userLeaveRequest, id, todaysDate, message);
		result.put("Message", message);
		return result;
	}
	
	// LISTS OF LEAVE REQUEST (PAGINATION)
	@GetMapping("/listRequest")
	public Map<String, Object> getListRequest(@RequestParam(name = "page") int page, 
			@RequestParam(name = "size") int size, @RequestParam(name = "userId") Long userId) {
		Map<String, Object> result = new HashMap<String, Object>();

		Pageable paging = PageRequest.of(page, size);
		List<UserLeaveRequest> pagedResult = userRequestRepo.findUserWithPagination(paging, userId);
		List<UserLeaveRequestDto> pagedResultDto = new ArrayList<UserLeaveRequestDto>();
		 
		for(UserLeaveRequest req : pagedResult) {
			UserLeaveRequestDto requestDto = modelMapper.map(req, UserLeaveRequestDto.class);
			pagedResultDto.add(requestDto);
		}
		result.put("Items", pagedResultDto);
		result.put("Total Items", pagedResult.size());
		return result;
	}
	
	public void createRequest(UserLeaveRequestDto body, Users userEntity, UserLeaveRequest userLeaveRequest,
			Long id, Date todaysDate, Map<String, Object> message) {
		PositionLeave positionLeaveEntity = getPositionLeaveByDate(body);
		int leaveAllowance = calculateLeaveAllowance(positionLeaveEntity, userEntity);
		int leaveDuration = calculateLeaveDuration(body);
		int remainingLeave = calculateRemainingLeave(userEntity, leaveAllowance, leaveDuration);
		boolean inputValidation = inputDateValidation(body, id, message, leaveDuration, userLeaveRequest, 
				remainingLeave, todaysDate);
		if (inputValidation == true) {
			setUserLeaveRequestEntity(userEntity, todaysDate, userLeaveRequest, remainingLeave);
		}
	}
	
	// GET POSITION LEAVE ENTITY BY DATE
	public PositionLeave getPositionLeaveByDate(UserLeaveRequestDto body) {
		List<PositionLeave> listAllPositionLeave = positionLeaveRepo.findAll();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
		String dateParameter = dateFormat.format(body.getLeaveDateFrom());
		PositionLeave positionLeaveEntity = null;
		for(PositionLeave positionLeave : listAllPositionLeave) {
			String datePositionLeave = dateFormat.format(positionLeave.getPositionLeaveDate());
			if (datePositionLeave.equals(dateParameter)) {
				positionLeaveEntity = positionLeave;
			}
		}
		return positionLeaveEntity;
	}
	
	// CALCULATE LEAVE ALLOWANCE FOR EVERY POSITION
	public int calculateLeaveAllowance(PositionLeave positionLeaveEntity, Users userEntity) {
		int leaveAllowance = 0;
		if (userEntity.getPosition().getPositionName().equalsIgnoreCase("Employee")) {
			leaveAllowance = positionLeaveEntity.getEmployeeLeave();
		}
		else if (userEntity.getPosition().getPositionName().equalsIgnoreCase("Supervisor")) {
			leaveAllowance = positionLeaveEntity.getSupervisorLeave();
		}
		else if (userEntity.getPosition().getPositionName().equalsIgnoreCase("Staff")) {
			leaveAllowance = positionLeaveEntity.getStaffLeave();
		}
		return leaveAllowance;
	}
	
	// CALCULATE LEAVE DURATION
	public int calculateLeaveDuration(UserLeaveRequestDto body) {
		Long duration = body.getLeaveDateTo().getTime() - body.getLeaveDateFrom().getTime();
		int leaveDuration = ((int) (duration/(24*60*60*1000)) + 1);
		return leaveDuration;
	}
	
	// CALCULATE REMAINING LEAVE
	public int calculateRemainingLeave(Users userEntity, int leaveAllowance, int leaveDuration) {
		int remainingLeave = leaveAllowance - userEntity.getLeaveTotal();
		return remainingLeave;
	}
	
	// REQUEST DATE & LEAVE DATE VALIDATION
	public boolean inputDateValidation(UserLeaveRequestDto body, Long id, Map<String, Object> message, int leaveDuration, 
			UserLeaveRequest userLeaveRequest, int remainingLeave, Date todaysDate) {
		boolean valid = false;
		if(body.getLeaveDateFrom().before(todaysDate) || body.getLeaveDateTo().before(todaysDate)) {
			throw new RequestDateInputException("(Error : Cuti Backdate, Tanggal Pengajuan Cuti < Tanggal Hari Ini)"
					+ " Tanggal yang Anda Ajukan Telah Lampau, Silakan Ganti Tanggal Pengajuan Cuti Anda.");
		}
		else if (leaveDuration > 0) {
			if (remainingLeave == 0) {
				throw new RemainingLeaveException("(Error : Jatah Cuti Habis) Mohon Maaf, Jatah Cuti Anda Telah Habis");
			}
			else if (leaveDuration > remainingLeave) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				String dateFrom = dateFormat.format(body.getLeaveDateFrom());
				String dateTo = dateFormat.format(body.getLeaveDateTo());
				throw new LacksOfLeaveAllowanceException(dateFrom, dateTo, leaveDuration, remainingLeave);
			}
			else {
				message.put("Success", "Permohonan Anda Sedang Diproses");
				valid = true;
			}
		} 
		else {
			throw new RequestDateInputException("(Error : Tanggal Salah, leaveDateFrom > leaveDateTo) "
					+ "Tanggal yang Anda Ajukan Tidak Valid. ");
		}
		return valid;
	}
	
	// SAVE TO ENTITY REPOSITORY
	public void setUserLeaveRequestEntity(Users userEntity, Date todaysDate, UserLeaveRequest userLeaveRequest, int remainingLeave) {
		userLeaveRequest.setUsers(userEntity);
		userLeaveRequest.setRemainingLeave(remainingLeave);
		userLeaveRequest.setRequestDate(todaysDate);
		userLeaveRequest.setRequestStatus("Waiting");
		userRequestRepo.save(userLeaveRequest);
	}
}
