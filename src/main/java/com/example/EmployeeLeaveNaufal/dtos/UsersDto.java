package com.example.EmployeeLeaveNaufal.dtos;

import java.util.Date;

public class UsersDto {
	private long userId;
	private PositionDto position;
	private String userName;
	private int leaveTotal;
	private String createdBy;
	private Date createdAt;
	private String updatedBy;
	private Date updatedAt;
	
	// Constructor
	public UsersDto() {
		// TODO Auto-generated constructor stub
	}

	public UsersDto(long userId, PositionDto position, String userName, int leaveTotal, String createdBy,
			Date createdAt, String updatedBy, Date updatedAt) {
		super();
		this.userId = userId;
		this.position = position;
		this.userName = userName;
		this.leaveTotal = leaveTotal;
		this.createdBy = createdBy;
		this.createdAt = createdAt;
		this.updatedBy = updatedBy;
		this.updatedAt = updatedAt;
	}

	// Getter Setter
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public PositionDto getPosition() {
		return position;
	}

	public void setPosition(PositionDto position) {
		this.position = position;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getLeaveTotal() {
		return leaveTotal;
	}

	public void setLeaveTotal(int leaveTotal) {
		this.leaveTotal = leaveTotal;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
