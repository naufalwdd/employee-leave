package com.example.EmployeeLeaveNaufal.exceptions;

public class DateValidationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4888113420760514755L;
	
	public DateValidationException(String message) {
		super(message);
	}
}
