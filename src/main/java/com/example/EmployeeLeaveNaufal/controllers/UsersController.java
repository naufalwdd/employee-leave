package com.example.EmployeeLeaveNaufal.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeLeaveNaufal.dtos.UsersDto;
import com.example.EmployeeLeaveNaufal.exceptions.ResourceNotFoundException;
import com.example.EmployeeLeaveNaufal.interfaces.Auditor;
import com.example.EmployeeLeaveNaufal.interfaces.LeaveTotal;
import com.example.EmployeeLeaveNaufal.models.Users;
import com.example.EmployeeLeaveNaufal.repositories.UsersRepository;

@RestController
@RequestMapping("/api/user")
public class UsersController implements Auditor, LeaveTotal {
	
	ModelMapper modelMapper = new ModelMapper();
	@Autowired
	UsersRepository userRepo;
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createUser(@Valid @RequestBody UsersDto body) {
		Map<String, Object> result = new HashMap<String, Object>();

		body.setCreatedBy(auditorName);
		body.setUpdatedBy(auditorName);
		body.setLeaveTotal(inputOfLeaveTotal);
		Users userEntity = modelMapper.map(body, Users.class);
		
		userRepo.save(userEntity);
		
		body.setUserId(userEntity.getUserId());
		result.put("Message", "User Created Successfully");
		result.put("Data", body);
		return result;
	}
	
	// READ ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllUser() {
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Users> listUserEntity = userRepo.findAll();
		List<UsersDto> listUserDto = new ArrayList<UsersDto>();
		
		for (Users userEntity : listUserEntity) {
			UsersDto userDto = modelMapper.map(userEntity, UsersDto.class);
			listUserDto.add(userDto);
		}
		
		result.put("Message", "All Users Read Successfully");
		result.put("Data", listUserDto);
		result.put("Total", listUserDto.size());
		
		return result;
	}
	
	// READ BY ID
	@GetMapping("/read")
	public Map<String, Object> getUserById(@RequestParam(name = "userId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Users userEntity = userRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Users", "userId", id));
		UsersDto userDto = modelMapper.map(userEntity, UsersDto.class);
		
		result.put("Message", "User Read Successfully");
		result.put("Data", userDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updateUser(@Valid @RequestBody UsersDto body, @RequestParam(name = "userId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Users userEntity = userRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Users", "userId", id));
		Date createdAt = userEntity.getCreatedAt();
		body.setCreatedBy(auditorName);
		body.setUpdatedBy(auditorName);
		body.setLeaveTotal(inputOfLeaveTotal);
		userEntity = modelMapper.map(body, Users.class);
		userEntity.setUserId(id);
		userEntity.setCreatedAt(createdAt);

		userRepo.save(userEntity);
		
		body.setUserId(userEntity.getUserId());
		result.put("Message", "User Updated Successfully");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deleteUser(@RequestParam(name = "userId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Users userEntity = userRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Users", "userId", id));
		
		UsersDto userDto = modelMapper.map(userEntity, UsersDto.class);
		
		userRepo.deleteById(id);
		
		result.put("Message", "User Deleted Successfully");
		result.put("Data Deleted", userDto);
		return result;
	}
	
}
