package com.example.EmployeeLeaveNaufal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.EmployeeLeaveNaufal.models.Users;

public interface UsersRepository extends JpaRepository<Users, Long>{

	@Query(value = "select user_name from users where user_id=:userId", nativeQuery=true)
	public String findUserName(Long userId);
}
