package com.example.EmployeeLeaveNaufal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeeLeaveNaufal.models.PositionLeave;

public interface PositionLeaveRepository extends JpaRepository<PositionLeave, Long> {

}
