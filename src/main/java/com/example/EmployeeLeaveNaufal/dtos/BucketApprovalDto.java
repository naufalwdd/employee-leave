package com.example.EmployeeLeaveNaufal.dtos;

import java.util.Date;

public class BucketApprovalDto {

	private long bucketApprovalId;
	private UserLeaveRequestDto userLeaveRequest;
	private UsersDto users;
	private Date resolveDate;
	private String resolveReason;
	private String createdBy;
	private Date createdAt;
	private String updatedBy;
	private Date updatedAt;
	
	// Constructor
	public BucketApprovalDto() {
		// TODO Auto-generated constructor stub
	}

	public BucketApprovalDto(long bucketApprovalId, UserLeaveRequestDto userLeaveRequest, UsersDto users,
			Date resolveDate, String resolveReason, String createdBy, Date createdAt, String updatedBy,
			Date updatedAt) {
		super();
		this.bucketApprovalId = bucketApprovalId;
		this.userLeaveRequest = userLeaveRequest;
		this.users = users;
		this.resolveDate = resolveDate;
		this.resolveReason = resolveReason;
		this.createdBy = createdBy;
		this.createdAt = createdAt;
		this.updatedBy = updatedBy;
		this.updatedAt = updatedAt;
	}

	// Getter Setter
	public long getBucketApprovalId() {
		return bucketApprovalId;
	}

	public void setBucketApprovalId(long bucketApprovalId) {
		this.bucketApprovalId = bucketApprovalId;
	}

	public UserLeaveRequestDto getUserLeaveRequest() {
		return userLeaveRequest;
	}

	public void setUserLeaveRequest(UserLeaveRequestDto userLeaveRequest) {
		this.userLeaveRequest = userLeaveRequest;
	}

	public UsersDto getUsers() {
		return users;
	}

	public void setUsers(UsersDto users) {
		this.users = users;
	}

	public Date getResolveDate() {
		return resolveDate;
	}

	public void setResolveDate(Date resolveDate) {
		this.resolveDate = resolveDate;
	}

	public String getResolveReason() {
		return resolveReason;
	}

	public void setResolveReason(String resolveReason) {
		this.resolveReason = resolveReason;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
