package com.example.EmployeeLeaveNaufal.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeLeaveNaufal.dtos.BucketApprovalDto;
import com.example.EmployeeLeaveNaufal.dtos.UserLeaveRequestDto;
import com.example.EmployeeLeaveNaufal.dtos.UsersDto;
import com.example.EmployeeLeaveNaufal.exceptions.DateValidationException;
import com.example.EmployeeLeaveNaufal.exceptions.RequestIdNotFoundException;
import com.example.EmployeeLeaveNaufal.exceptions.ResolverPositionException;
import com.example.EmployeeLeaveNaufal.interfaces.Auditor;
import com.example.EmployeeLeaveNaufal.models.BucketApproval;
import com.example.EmployeeLeaveNaufal.models.UserLeaveRequest;
import com.example.EmployeeLeaveNaufal.models.Users;
import com.example.EmployeeLeaveNaufal.repositories.BucketApprovalRepository;
import com.example.EmployeeLeaveNaufal.repositories.UserLeaveRequestRepository;
import com.example.EmployeeLeaveNaufal.repositories.UsersRepository;

@RestController
@RequestMapping("api/bucket")
public class BucketApprovalController implements Auditor {
	ModelMapper modelMapper = new ModelMapper();
	@Autowired
	BucketApprovalRepository bucketRepo;
	@Autowired
	UsersRepository userRepo;
	@Autowired
	UserLeaveRequestRepository userRequestRepo;
	
	@PostMapping("/resolve")
	public Map<String, Object> resolveRequest(@Valid @RequestBody BucketApprovalDto body, 
			@RequestParam(name = "userId") Long userId, @RequestParam(name = "requestId") Long requestId) {
		
		// Create Output Template
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> response = new HashMap<String, Object>();
		
		// Initialize Date
		Date todaysDate = new Date();
		
		// Get Request By Id
		UserLeaveRequest request = userRequestRepo.findById(requestId)
				.orElseThrow(() -> new RequestIdNotFoundException(requestId));
		
		// Set User Id
		String resolverName = userRepo.findUserName(userId);
		UsersDto userDto = new UsersDto();
		userDto.setUserId(userId);
		
		// Set User Leave Request Id
		UserLeaveRequestDto requestDto = new UserLeaveRequestDto();
		requestDto.setUserLeaveRequestId(requestId);
		
		// Create Body
		body.setUsers(userDto);
		body.setUserLeaveRequest(requestDto);
		body.setResolveDate(todaysDate);
		body.setCreatedBy(auditorName);
		body.setUpdatedBy(auditorName);
		BucketApproval approval = modelMapper.map(body, BucketApproval.class);
		body.setBucketApprovalId(approval.getBucketApprovalId());
		
		// Data Processing Flow
		createBucket(body, userId, requestId, response, request, todaysDate, approval);
		
		// Result
		result.put("Response", response);
		result.put("Resolved By", resolverName);
		return result;
	}
	
	// DATA PROCESSING FLOW
	public void createBucket(BucketApprovalDto body, Long userId, Long requestId, Map<String, Object> response, 
			UserLeaveRequest request, Date todaysDate, BucketApproval approval) {
		int dateValidation = resolveDateValidation(request, todaysDate);
		UserLeaveRequest userRequestById = findRequestById(requestId);
		Users requestResolver = findResolver(userId);
		int leaveDuration = calculateLeaveDuration(userRequestById);
		boolean execute = getResponse(body, userId, requestId, response, userRequestById, todaysDate,
				approval, dateValidation, leaveDuration, requestResolver);
		if (execute == true) {
			createEntity(body, requestId, userRequestById, leaveDuration, approval, todaysDate);
		}
	}
	
	// RESOLVE DATE AND REQUEST DATE VALIDATION
	public int resolveDateValidation(UserLeaveRequest request, Date todaysDate) {
		int dateValidation = (int) (request.getLeaveDateFrom().getTime() - todaysDate.getTime());
		return dateValidation;
	}
	
	// FIND REQUEST BY ID
	public UserLeaveRequest findRequestById(Long requestId) {
		List<UserLeaveRequest> listOfWaitingRequest = userRequestRepo.findAll();
		UserLeaveRequest userRequestById = new UserLeaveRequest();
		for (UserLeaveRequest req : listOfWaitingRequest) {
			if (req.getUserLeaveRequestId() == requestId) {
				userRequestById = req;
			}
		}
		return userRequestById;
	}
	
	// FIND WAITING REQUEST
	public List<UserLeaveRequest> findWaitingRequest() {
		List<UserLeaveRequest> allRequest = userRequestRepo.findAll();
		List<UserLeaveRequest> listOfWaitingRequest = new ArrayList<UserLeaveRequest>();
		for (UserLeaveRequest req : allRequest) {
			if (req.getRequestStatus().equalsIgnoreCase("Waiting")) {
				listOfWaitingRequest.add(req);
			}
		}
		return listOfWaitingRequest;
	}
	
	// FIND REQUEST RESOLVER
	public Users findResolver(Long userId) {
		List<Users> listAllUser = userRepo.findAll();
		Users resolverEntity = new Users();
		for (Users resolver : listAllUser) {
			if (resolver.getUserId() == userId) {
				resolverEntity = resolver;
			}
		}
		return resolverEntity;
	}
	
	// CALCULATE LEAVE DURATION
	public int calculateLeaveDuration(UserLeaveRequest userRequestById) {
		Long duration = userRequestById.getLeaveDateTo().getTime() - userRequestById.getLeaveDateFrom().getTime();
		int leaveDuration = (int) ((duration/(24*60*60*1000) + 1));
		return leaveDuration;
	}
	
	// GET RESPONSE
	public boolean getResponse(BucketApprovalDto body, Long userId, Long requestId, Map<String, Object> response,
			UserLeaveRequest userRequestById, Date todaysDate, BucketApproval approval, int dateValidation, int leaveDuration,
			Users requestResolver) {
		boolean execute = false;
		String resolver = getResolverPosition(requestResolver);
		String requester = getRequesterPosition(userRequestById);
		
		if (dateValidation <= 0) {
			throw new DateValidationException("(Error : resolvedDate < leaveDateFrom). Kesalahan data, tanggal "
					+ "keputusan tidak bisa lebih awal dari pengajuan cuti.");
		}
		
		// Resolver : Supervisor
		else if (resolver.equalsIgnoreCase("Supervisor")) {
			if ((requester.equalsIgnoreCase("Supervisor") && userRequestById.getUsers().getUserId() == requestResolver.getUserId()) 
					|| requester.equalsIgnoreCase("Staff")) {
				throw new ResolverPositionException("Supervisor hanya dapat melakukan approve terhadap sesama "
						+ "Supervisor, namun tidak dapat melakukan approve terhadap dirinya sendiri. ");
			}
			else if ((requester.equalsIgnoreCase("Employee") || requester.equalsIgnoreCase("Staff")) && 
					!userRequestById.getRequestStatus().equalsIgnoreCase("Waiting")) {
				response.put("Warning", "Permohonan dengan ID " + requestId + " sudah pernah di-resolve sebelumnya.");
			}
			else if (requester.equalsIgnoreCase("Supervisor") && 
					userRequestById.getUsers().getUserId() != requestResolver.getUserId() && 
					!userRequestById.getRequestStatus().equalsIgnoreCase("Waiting")) {
				response.put("Warning", "Permohonan dengan ID " + requestId + " sudah pernah di-resolve sebelumnya.");
			}
			else {
				response.put("Success", "Permohonan dengan ID " + requestId + " telah berhasil diputuskan.");
				execute = true;
			}
		}
		
		// Resolver : Staff
		else if (resolver.equalsIgnoreCase("Staff")) {
			if (requester.equalsIgnoreCase("Supervisor") || requester.equalsIgnoreCase("Employee")) {
				throw new ResolverPositionException("Staff hanya dapat melakukan approve terhadap sesama Staff"
					+ " dan dirinya sendiri.");
			}
			else if (requester.equalsIgnoreCase("Staff") && !userRequestById.getRequestStatus().equalsIgnoreCase("Waiting")) {
				response.put("Warning", "Permohonan dengan ID " + requestId + " sudah pernah di-resolve sebelumnya.");
			}
			else {
				response.put("Success", "Permohonan dengan ID " + requestId + " telah berhasil diputuskan.");
				execute = true;
			}
		}
		
		// Resolver : Employee
		else if (resolver.equalsIgnoreCase("Employee")) {
			throw new ResolverPositionException("Employee tidak dapat melakukan approve. ");
		}
		return execute;
	}
	
	// GET RESOLVER POSITION
	public String getResolverPosition(Users requestResolver) {
		String resolverPosition = "";
		if (requestResolver.getPosition().getPositionName().equalsIgnoreCase("Supervisor")) {
			resolverPosition = "Supervisor";
		}
		else if (requestResolver.getPosition().getPositionName().equalsIgnoreCase("Staff")) {
			resolverPosition = "Staff";
		}
		else if (requestResolver.getPosition().getPositionName().equalsIgnoreCase("Employee")) {
			resolverPosition = "Employee";
		}
		return resolverPosition;
	}
	
	// GET REQUESTER POSITION
	public String getRequesterPosition(UserLeaveRequest userRequestById) {
		String requesterPosition = "";
		if (userRequestById.getUsers().getPosition().getPositionName().equalsIgnoreCase("Supervisor")) {
			requesterPosition = "Supervisor";
		}
		if (userRequestById.getUsers().getPosition().getPositionName().equalsIgnoreCase("Staff")) {
			requesterPosition = "Staff";
		}
		if (userRequestById.getUsers().getPosition().getPositionName().equalsIgnoreCase("Employee")) {
			requesterPosition = "Employee";
		}
		return requesterPosition;
	}
	
	// SAVE ENTITY TO REPOSITORY
	public void createEntity(BucketApprovalDto body, Long requestId, UserLeaveRequest userRequestById,
			int leaveDuration, BucketApproval approval, Date todaysDate) {
		
		// WHEN REQUEST APPROVED :
		if (body.getResolveReason().equalsIgnoreCase("Dipersilahkan")) {
			// Set User Entity
			Users user = userRepo.findById(userRequestById.getUsers().getUserId()).get();
			user.setLeaveTotal(user.getLeaveTotal() + leaveDuration);
			user.setUserId(userRequestById.getUsers().getUserId());
			
			// Set User Leave Request Entity
			UserLeaveRequest request = userRequestRepo.findById(requestId).get();
			request.setUserLeaveRequestId(requestId);
			request.setRequestStatus("Approved");
			request.setRemainingLeave(request.getRemainingLeave() - leaveDuration);
			
			// Set Bucket Approval Entity
			approval.setResolveDate(todaysDate);
			approval.setResolveReason("Dipersilahkan");
			approval.setUserLeaveRequest(request);
			approval.setUsers(user);
			bucketRepo.save(approval);
		}
		
		// WHEN REQUEST REJECTED
		else {
			// Set User Leave Request Entity
			UserLeaveRequest request = userRequestRepo.findById(requestId).get();
			request.setUserLeaveRequestId(requestId);
			request.setRequestStatus("Rejected");
			
			// Set User Entity
			Users user = userRepo.findById(userRequestById.getUsers().getUserId()).get();
			user.setUserId(userRequestById.getUsers().getUserId());
			
			// Set Bucket Approval Entity
			approval.setResolveDate(todaysDate);
			approval.setResolveReason(body.getResolveReason());
			approval.setUserLeaveRequest(request);
			approval.setUsers(user);
			bucketRepo.save(approval);
		}
	}
}
