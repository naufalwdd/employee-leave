package com.example.EmployeeLeaveNaufal.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.EmployeeLeaveNaufal.models.BucketApproval;

public interface BucketApprovalRepository extends JpaRepository<BucketApproval, Long> {

	@Query(value = "select * from bucket_approval\r\n"
			+ "where user_id in (select user_id from users where\r\n"
			+ "				 position_id in (select position_id from position where position_name=:position))", nativeQuery=true)
	public List<BucketApproval> findBucketApprovalByPosition(String position);
}
