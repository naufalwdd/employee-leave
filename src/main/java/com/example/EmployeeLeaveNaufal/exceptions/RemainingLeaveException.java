package com.example.EmployeeLeaveNaufal.exceptions;

public class RemainingLeaveException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6347827177706647687L;
	
	public RemainingLeaveException(String message) {
		super(message);
	}
}
