package com.example.EmployeeLeaveNaufal.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.EmployeeLeaveNaufal.models.UserLeaveRequest;

public interface UserLeaveRequestRepository extends JpaRepository<UserLeaveRequest, Long>, PagingAndSortingRepository<UserLeaveRequest, Long> {
	// PAGINATION
	@Query(value = "SELECT * FROM user_leave_request WHERE user_id=:userId", 
			  nativeQuery = true)
	public List<UserLeaveRequest> findUserWithPagination(Pageable pageable, @RequestParam("userId") Long userId);
	
	// LIST OF REQUEST BY STATUS
	@Query(value = "SELECT * FROM user_leave_request WHERE request_status=:status",
			nativeQuery = true)
	public List<UserLeaveRequest> findApprovedRequest(String status);
}
