package com.example.EmployeeLeaveNaufal.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeLeaveNaufal.dtos.PositionDto;
import com.example.EmployeeLeaveNaufal.exceptions.ResourceNotFoundException;
import com.example.EmployeeLeaveNaufal.interfaces.Auditor;
import com.example.EmployeeLeaveNaufal.models.Position;
import com.example.EmployeeLeaveNaufal.repositories.PositionRepository;

@RestController
@RequestMapping("/api/position")
public class PositionController implements Auditor {

	ModelMapper modelMapper = new ModelMapper();
	@Autowired
	PositionRepository positionRepo;
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createPosition(@Valid @RequestBody PositionDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		body.setCreatedBy(auditorName);
		body.setUpdatedBy(auditorName);
		Position positionEntity = modelMapper.map(body, Position.class);
			
		positionRepo.save(positionEntity);
		body.setPositionId(positionEntity.getPositionId());
		result.put("Message", "Position Created Successfully");
		result.put("Data", body);
		return result;
	}
	
	// READ ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllPosition() {
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Position> listPositionEntity = positionRepo.findAll();
		List<PositionDto> listPositionDto = new ArrayList<PositionDto>();
		
		for (Position positionEntity : listPositionEntity) {
			PositionDto positionDto = modelMapper.map(positionEntity, PositionDto.class);
			listPositionDto.add(positionDto);
		}
		
		result.put("Message", "All Positions Read Successfully");
		result.put("Data", listPositionDto);
		result.put("Total", listPositionDto.size());
		
		return result;
	}
	
	// READ BY ID
	@GetMapping("/read")
	public Map<String, Object> getPositionById(@RequestParam(name = "positionId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Position positionEntity = positionRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Position", "positionId", id));
		PositionDto positionDto = modelMapper.map(positionEntity, PositionDto.class);
		
		result.put("Message", "Position Read Successfully");
		result.put("Data", positionDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updatePosition(@Valid @RequestBody PositionDto body, @RequestParam(name = "positionId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Position positionEntity = positionRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Position", "positionId", id));
		Date createdAt = positionEntity.getCreatedAt();
		body.setCreatedBy(auditorName);
		body.setUpdatedBy(auditorName);
		positionEntity = modelMapper.map(body, Position.class);
		positionEntity.setPositionId(id);
		positionEntity.setCreatedAt(createdAt);
		positionRepo.save(positionEntity);
		
		body.setPositionId(positionEntity.getPositionId());
		result.put("Message", "Position Updated Successfully");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deletePosition(@RequestParam(name = "positionId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Position positionEntity = positionRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Position", "positionId", id));
		PositionDto positionDto = modelMapper.map(positionEntity, PositionDto.class);
		
		positionRepo.deleteById(id);
		result.put("Message", "Position Deleted Successfully");
		result.put("Data Deleted", positionDto);
		return result;
	}
}
