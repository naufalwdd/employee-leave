package com.example.EmployeeLeaveNaufal.exceptions;

public class RequestDateInputException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9028808430025159258L;
	
	public RequestDateInputException(String message) {
		super(message);
	}
}
