package com.example.EmployeeLeaveNaufal.dtos;

import java.util.Date;

public class PositionLeaveDto {
	
	private long positionLeaveId;
	private int employeeLeave;
	private int supervisorLeave;
	private int staffLeave;
	private String createdBy;
	private Date createdAt;
	private String updatedBy;
	private Date updatedAt;
	private Date positionLeaveDate;
	
	// Constructor
	public PositionLeaveDto() {
		// TODO Auto-generated constructor stub
	}

	public PositionLeaveDto(long positionLeaveId, int employeeLeave, int supervisorLeave, int staffLeave,
			String createdBy, Date createdAt, String updatedBy, Date updatedAt, Date positionLeaveDate) {
		super();
		this.positionLeaveId = positionLeaveId;
		this.employeeLeave = employeeLeave;
		this.supervisorLeave = supervisorLeave;
		this.staffLeave = staffLeave;
		this.createdBy = createdBy;
		this.createdAt = createdAt;
		this.updatedBy = updatedBy;
		this.updatedAt = updatedAt;
		this.positionLeaveDate = positionLeaveDate;
	}

	// Getter Setter
	public long getPositionLeaveId() {
		return positionLeaveId;
	}

	public void setPositionLeaveId(long positionLeaveId) {
		this.positionLeaveId = positionLeaveId;
	}

	public int getEmployeeLeave() {
		return employeeLeave;
	}

	public void setEmployeeLeave(int employeeLeave) {
		this.employeeLeave = employeeLeave;
	}

	public int getSupervisorLeave() {
		return supervisorLeave;
	}

	public void setSupervisorLeave(int supervisorLeave) {
		this.supervisorLeave = supervisorLeave;
	}

	public int getStaffLeave() {
		return staffLeave;
	}

	public void setStaffLeave(int staffLeave) {
		this.staffLeave = staffLeave;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Date getPositionLeaveDate() {
		return positionLeaveDate;
	}

	public void setPositionLeaveDate(Date positionLeaveDate) {
		this.positionLeaveDate = positionLeaveDate;
	}
	
}
