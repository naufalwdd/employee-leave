package com.example.EmployeeLeaveNaufal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class EmployeeLeaveNaufalApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeLeaveNaufalApplication.class, args);
	}

}
