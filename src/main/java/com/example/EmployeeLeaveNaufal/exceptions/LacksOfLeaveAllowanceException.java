package com.example.EmployeeLeaveNaufal.exceptions;

public class LacksOfLeaveAllowanceException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7252028058577303131L;

	public LacksOfLeaveAllowanceException(String dateFrom, String dateTo, int leaveDuration, int remainingLeave) {
		super(String.format("(Error : Jatah Cuti Tidak Cukup) "
				+ "Mohon Maaf, Jatah Cuti Anda Tidak Cukup Untuk Digunakan dari Tanggal "
				+"%s Sampai %s (%d Hari). Jatah Cuti Anda yang Tersisa adalah %d hari. ",
				dateFrom, dateTo, leaveDuration, remainingLeave));
	}
	
}
