package com.example.EmployeeLeaveNaufal.exceptions;

public class RequestIdNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3751112665834803570L;

	public RequestIdNotFoundException(Long requestId) {
		super(String.format("(Error : bucketApprovalId tidak ada) "
				+ "Permohonan dengan ID %d tidak ditemukan.", requestId));
	}
}
