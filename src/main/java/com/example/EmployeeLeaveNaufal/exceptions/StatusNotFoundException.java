package com.example.EmployeeLeaveNaufal.exceptions;

public class StatusNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5266215873122552042L;

	public StatusNotFoundException(String message) {
		super(message);
	}
}
