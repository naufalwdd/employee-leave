package com.example.EmployeeLeaveNaufal.customAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* +===============================+
 * | ANSWER FOR QUESTION NUMBER 5  |
 * | AUTHOR: MUHAMMAD NAUFAL WIDAD |
 * | BOOTCAMP - G23                |
 * | >>>>>>>> CUSTOM API <<<<<<<<< |
 * +===============================+
 */

import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeLeaveNaufal.dtos.BucketApprovalDto;
import com.example.EmployeeLeaveNaufal.dtos.UserLeaveRequestDto;
import com.example.EmployeeLeaveNaufal.exceptions.StatusNotFoundException;
import com.example.EmployeeLeaveNaufal.models.BucketApproval;
import com.example.EmployeeLeaveNaufal.models.UserLeaveRequest;
import com.example.EmployeeLeaveNaufal.repositories.BucketApprovalRepository;
import com.example.EmployeeLeaveNaufal.repositories.UserLeaveRequestRepository;

@RestController
@RequestMapping("api/custom")
public class CustomAPIController {

	ModelMapper modelMapper = new ModelMapper();
	@Autowired
	UserLeaveRequestRepository userRequestRepo;
	@Autowired
	BucketApprovalRepository bucketRepo;
	
	// CREATE LIST OF USER LEAVE REQUEST BY STATUS
	@GetMapping("/requestListByStatus")
	public Map<String, Object> readApprovedRequest(@RequestParam(name = "status") String status) {
		String input = status;
		String firstLetter = input.substring(0,1);
		String remainingLetters = input.substring(1, input.length());
		firstLetter = firstLetter.toUpperCase();
		input = firstLetter + remainingLetters;
		
		Map<String, Object> result = new HashMap<String, Object>();
		List<UserLeaveRequest> listRequestEntity = userRequestRepo.findApprovedRequest(input);
		List<UserLeaveRequestDto> listRequestDto = new ArrayList<UserLeaveRequestDto>();
		
		for (UserLeaveRequest userRequestEntity : listRequestEntity) {
			UserLeaveRequestDto userRequestDto = modelMapper.map(userRequestEntity, UserLeaveRequestDto.class);
			listRequestDto.add(userRequestDto);
		}
		if (status.equalsIgnoreCase("waiting") || status.equalsIgnoreCase("approved") || status.equalsIgnoreCase("rejected")) {
			if (listRequestDto.isEmpty()) {
				result.put("Message", "Data tidak ditemukan.");
			}
			else {
				result.put("Message", "List of Approved Request Has Been Displayed Successfully.");
				result.put("Data", listRequestDto);
			}
		}
		else {
			throw new StatusNotFoundException("Anda salah memasukkan input. ");
		}
		return result;
	}
	
	// CREATE LIST OF BUCKET APPROVAL BY POSITION
	@GetMapping("/bucketApprovalByPosition")
	public Map<String, Object> readStaffBucketApproval(@RequestParam(name = "position") String position) {
		String input = position;
		String firstLetter = input.substring(0,1);
		String remainingLetters = input.substring(1, input.length());
		firstLetter = firstLetter.toUpperCase();
		input = firstLetter + remainingLetters;
		
		Map<String, Object> result = new HashMap<String, Object>();
		List<BucketApproval> listBucketApprovalEntity = bucketRepo.findBucketApprovalByPosition(input);
		List<BucketApprovalDto> listBucketApprovalDto = new ArrayList<BucketApprovalDto>();
		for (BucketApproval bucketApprovalEntity : listBucketApprovalEntity) {
			BucketApprovalDto bucketApprovalDto = modelMapper.map(bucketApprovalEntity, BucketApprovalDto.class);
			listBucketApprovalDto.add(bucketApprovalDto);
		}
		if (position.equalsIgnoreCase("staff") || position.equalsIgnoreCase("employee") || 
				position.equalsIgnoreCase("supervisor")) {
			if (listBucketApprovalDto.isEmpty()) {
				result.put("Message", "Data tidak ditemukan.");
			}
			else {
				result.put("Message", "List of Approved Request Has Been Displayed Successfully.");
				result.put("Data", listBucketApprovalDto);
			}
		}
		else {
			throw new StatusNotFoundException("Anda salah memasukkan input. ");
		}
		return result;
	}
}
