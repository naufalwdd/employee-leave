package com.example.EmployeeLeaveNaufal.dtos;

import java.util.Date;

public class UserLeaveRequestDto {

	private long userLeaveRequestId;
	private UsersDto users;
	private Date requestDate;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private String description;
	private int remainingLeave;
	private String requestStatus;
	private String createdBy;
	private Date createdAt;
	private String updatedBy;
	private Date updatedAt;
	
	// Constructor
	public UserLeaveRequestDto() {
		// TODO Auto-generated constructor stub
	}

	public UserLeaveRequestDto(long userLeaveRequestId, UsersDto users, Date requestDate, Date leaveDateFrom,
			Date leaveDateTo, String description, int remainingLeave, String requestStatus, String createdBy,
			Date createdAt, String updatedBy, Date updatedAt) {
		super();
		this.userLeaveRequestId = userLeaveRequestId;
		this.users = users;
		this.requestDate = requestDate;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.description = description;
		this.remainingLeave = remainingLeave;
		this.requestStatus = requestStatus;
		this.createdBy = createdBy;
		this.createdAt = createdAt;
		this.updatedBy = updatedBy;
		this.updatedAt = updatedAt;
	}

	// Getter Setter
	public long getUserLeaveRequestId() {
		return userLeaveRequestId;
	}

	public void setUserLeaveRequestId(long userLeaveRequestId) {
		this.userLeaveRequestId = userLeaveRequestId;
	}

	public UsersDto getUsers() {
		return users;
	}

	public void setUsers(UsersDto users) {
		this.users = users;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	public Date getLeaveDateTo() {
		return leaveDateTo;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getRemainingLeave() {
		return remainingLeave;
	}

	public void setRemainingLeave(int remainingLeave) {
		this.remainingLeave = remainingLeave;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
