package com.example.EmployeeLeaveNaufal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeeLeaveNaufal.models.Position;

public interface PositionRepository extends JpaRepository<Position, Long>{

}
